package com.journaldev.spring.jdbc.dao;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.journaldev.spring.jdbc.model.Customer;

public class CustomerDAOImpl implements CustomerDAO {

	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public void create(Customer customer) {
		String queryCustomer = "insert into Customer_txn (id, name) values (?,?)";
		String queryAddress = "insert into Address_txn (id, address,country) values (?,?,?)";

		/*
		 * try { Connection conn = dataSource.getConnection(); Statement stmt =
		 * conn.createStatement(); stmt.execute(queryCustomer);
		 * stmt.executeQuery(queryAddress); } catch (SQLException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); }
		 */

		//JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

		jdbcTemplate.update(queryCustomer, new Object[] { customer.getId(), customer.getName() });
		System.out.println("Inserted into Customer Table Successfully");
		jdbcTemplate.update(queryAddress, new Object[] { customer.getId(), customer.getAddress().getAddress(),
				customer.getAddress().getCountry() });

		System.out.println("Inserted into Address Table Successfully");
	}

}
